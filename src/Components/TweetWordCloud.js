import React, {Component} from 'react';
import WordCloud from 'react-d3-cloud';
 
const fontSizeMapper = word => Math.log2(word.value) * 20;
const rotate = word => word.value % 360;

class TweetWordCloud extends Component {
    constructor(props){
        super(props);
        this.state = {
            data: props.data
        }
    }
    render(){
        return (
            <section id="word-cloud" className="bg-dark">
                <div className="container text-center" style={{marginBottom:'20px'}}>
                    <div className="row">
                        <div className="col-lg-6 mx-auto">
                            <h1 className="display-3" style={{color: '#ffffff'}}>
                                Word Cloud 
                            </h1>
                        </div>
                    </div>
                </div>
                <div className="container text-center"> 
                    <div className="row">   
                        <div className="col-lg-12 mx-auto">
                            <WordCloud
                                data={this.state.data}
                                fontSizeMapper={fontSizeMapper}
                                rotate={rotate}
                            />
                        </div> 
                    </div>
                </div>
            </section>
        );
    }
}

export default TweetWordCloud
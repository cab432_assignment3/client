import React from 'react';

export default function Header(props) {
  return ( 
        // <div className="jumbotron" style={{backgroundColor: '#007eff', color: '#ffffff'}}>
        //   <div className="container text-center" >
        //       <p className="display-1">React And Twitter</p>      
        //       <p className="lead">A flexible and beautiful Select Input control for ReactJS with multiselect, autocomplete and ajax support.</p>
        //   </div>
        // </div>
        <header className="bg-primary text-white" 
          style={{backgroundColor: '#007eff', color: '#ffffff',height: "341px"}}>
          <div className="jumbotron" style={{backgroundColor: '#007eff', color: '#ffffff'}}>
            <div className="container text-center" >
                <p className="display-1">Real-time Twitter Sentiment Analysis</p>      
                <p className="lead">This is a sentiment analysis solution for social media analytics by bringing real-time Twitter events</p>
            </div>
          </div>
        </header>
  )
}
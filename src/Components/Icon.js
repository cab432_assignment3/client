import React from 'react';

export default function Icon(props) {
    return (
        <div className="container" style={{marginBottom:'30px', marginTop:'50px'}}>
        <div className="row">  
        <div className="col-lg-7 mx-auto">   
            <h1 className="text-center">Instruction</h1>
            <p>You can query multiple keywords at once, and combine keywords in different ways. For example, to search for tweets with the words "cat" and "dog", enter: cat dog. To search for tweets with the words "cat" or "dog", enter: cat,dog.</p>
            <p>You need to specifiy how long you'd like to get tweets.</p>
            {/* <i className="fab fa-twitter-square " style={{fontSize: '82px',color:'rgb(0, 126, 255)'}}></i> */}
        </div>  
        </div>
        </div>
    );
}
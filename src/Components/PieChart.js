import React, { Component } from 'react';
import {Doughnut} from 'react-chartjs-2';

class PieChart extends Component {
    constructor(props){
        super(props);
        const data = props.chartData;
        this.state = {
            // chartData: props.chartData
            chartData: {
                labels:['apple','Facebook','Amazon','Google','Microsoft'],
                datasets:[
                    {
                        label: 'num of tweets',
                        data:[
                            data.apple.num,
                            data.facebook.num,
                            data.amazon.num,
                            data.google.num,
                            data.microsoft.num
                        ],
                        // backgroundColor:[
                        //     "rgba(0,10,220,0.5)",
                        //     "rgba(255,153,0,0.6)",
                        //     "rgba(220,0,0,0.5)",
                        //     "rgba(120,250,120,0.5)",
                        //     "rgba(200,150,220,0.5)"
                        // ],
                        backgroundColor: ["#F08080", "#dc3545","#ffc107","#007bff","#28a745"],
                        hoverBackgroundColor: 'rgba(255,99,132,0.4)',
                        // hoverBorderColor: 'rgba(255,99,132,1)',
                    }
                ]
            }
        }
    }
    static defaultProps = {
        displayTitle: true,
        displayLegend: true,
        legendPostion: 'bottom'
    }
    
    render(){
        return (
            <div className="chart">
                <Doughnut
                    data = {this.state.chartData}
                    options = {{
                        title: {
                            display: this.props.displayTitle,
                            text:'Number of Tweets',
                            fontSize: 25
                        },
                        legend: {
                            display: this.props.displayLegend,
                            position: this.props.legendPostion
                        },
                    }}
                />
            </div>
        )
    }
}


export default PieChart
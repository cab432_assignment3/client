import React, {Component} from 'react';
import {fetchTweets} from '../api/tweet';
import BarChart from './BarChart';
// import PieChart from './PieChart';
import TweetWordCloud from './TweetWordCloud';
import Icon from './Icon';
import socketIOClient from "socket.io-client";
import CountDown from './CountDown';

export default class Tweet extends Component {
    constructor(props) {
        super(props);
        this.timer = null;
        this.state = {
            topicScores: {
                "apple": {},
                "google": {},
                "amazon": {},
                "facebook": {},
                "microsoft": {}
            },
            value: 'apple',
            waitTime: '0',
            chartData:{},
            wordCloudData:[{}],
            isLoaded: false
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleClick = this.handleClick.bind(this);
        this.getData = this.getData.bind(this);
    }

    componentDidMount() {
        fetchTweets(this.state.value).then(data => {
            this.setState({topicScores: data.topices_scores});
            this.setState({wordCloudData: data.top_words});
            this.setState({isLoaded: true});
        })

        const socket = socketIOClient('http://localhost:3001/');

        socket.on('connect', () => {
            console.log("Socket Connected");
            // socket.emit('chat message', {data: 'yes'});
            socket.on("tweets", data => {
                //console.info(data);
                // let newList = [data].concat(this.state.items.slice(0, 15));
                // this.setState({ items: newList });
            });
        });
        socket.on('disconnect', () => {
            socket.off("tweets")
            socket.removeAllListeners("tweets");
            console.log("Socket Disconnected");
        });
    }
        
    handleChange(event){
        const target = event.target;
        // const value = target.type === 'checkbox' ? target.checked : target.value;
        const value = target.value;
        const name = target.name;

        this.setState({[name]: value});
        console.log(name, value);
        // this.setState({value: event.target.value});
    }

    handleClick() {
        clearTimeout(this.timer);  
        this.getData();
    }

    handleSubmit(event) {
        event.preventDefault();
        let value = this.state.value;
        let waitTime = this.state.waitTime;
        this.setState({isLoaded: false})
        
        fetch("http://localhost:3001/setSearchTerm",
        {
            method: "POST",
            headers: {
            'Content-Type': 'application/json'
            },
            body: JSON.stringify({ term: value })
        })

        // fetch("http://localhost:3001/start",
        // {
        //     method: "POST",
        //     headers: {
        //     'Content-Type': 'application/json'
        //     }
        // });
        this.timer = setTimeout(this.getData, parseInt(waitTime) * 1000);
    }

    getData(){
        fetchTweets(this.state.value).then(data => {
            this.setState({topicScores: data.topices_scores});
            this.setState({wordCloudData: data.top_words});
            this.setState({isLoaded: true});
        });
        fetch("http://localhost:3001/pause",
        {
            method: "POST",
            headers: {
            'Content-Type': 'application/json'
            }
        });
    }
    render() {
        let topices_scores = this.state.topicScores;
        let wordCloudData = this.state.wordCloudData;
        let sum = 0;
        
        Object.entries(topices_scores).forEach(
            // ([key, value]) => sum += value.num
            ([key, value]) => sum += value
        );
        
        if (!this.state.isLoaded) {
            return  <section id="loading">
                        <div className="container text-center" style={{marginTop:'50px'}}>
                            <div className="row">
                                <div className="col-lg-6 mx-auto">
                                    {this.state.waitTime === '0' ?
                                    <div/>:
                                    <div>
                                    <CountDown time={this.state.waitTime} />
                                    <br/><br/>
                                    <button onClick={this.handleClick} className="btn btn-primary" 
                                    sytle={{paddingLeft: '40px;'}}>Cancel</button>
                                    </div>}
                                </div>
                            </div>
                        </div>
                    </section>;
        }
        
        return (
            <React.Fragment>
                <section id="query-tweets">
                    <Icon />
                    <div className="container"> 
                        <div className="row">
                            <div className="col-lg-7 mx-auto">
                                <form onSubmit={this.handleSubmit}>
                                    <div className="form-group">
                                        {/* <label>How many tweets would you like to receive for analysis?</label>
                                        <select className="form-control form-control-sm"
                                                name="value"
                                                value={this.state.value} 
                                                onChange={this.handleChange}>
                                            <option value="#apple">Small </option>
                                            <option value="#google">Medium</option>
                                            <option value="#apple,#google,#amazon,#facebook,#microsoft">Large</option>
                                        </select> */}
                                        <label>Keywords</label>
                                        <input className="form-control form-control-sm"
                                                type="text"
                                                name="value"
                                                value={this.state.value} 
                                                onChange={this.handleChange} 
                                                required/>
                                    </div>
                                    
                                    <div className="form-group">
                                        <label>Time (seconds)</label>
                                        <input className="form-control form-control-sm"
                                                type="text"
                                                name="waitTime"
                                                value={this.state.waitTime} 
                                                onChange={this.handleChange} 
                                                required/>
                                    </div>
                                <input className="btn btn-outline-primary" type="submit" value="Submit"/>
                                </form>
                            </div>
                        </div>
                    </div>
                </section>
                <section id="results" className="bg-light">
                    <div className="container text-center" style={{marginBottom:'60px'}}>
                        <div className="row">
                            <div className="col-lg-10 mx-auto">
                                <h1 className="display-4">Number of Tweets: <mark>{sum}</mark></h1>
                            </div>
                        </div>
                    </div>

                    <div className="container"> 
                        <div className="row">   
                            <div className="col-lg-8 mx-auto"><BarChart chartData={topices_scores} redraw/> </div> 
                            {/* <div className="col-lg-6 mx-auto"><PieChart chartData={topices_scores} redraw/> </div>  */}
                        </div>
                    </div>
                </section>
                <TweetWordCloud  data={wordCloudData} />
            </React.Fragment>
        )
    }
}
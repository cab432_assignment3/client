import React from 'react';
import ReactCountdownClock from 'react-countdown-clock';
export default function CountDown(props) {
    let time = props.time;
    return ( 
        <ReactCountdownClock seconds={time}
        color="rgb(0, 126, 255)"
        alpha={0.9}
        size={275} />
    )
}
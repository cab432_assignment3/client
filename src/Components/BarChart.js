import React, { Component } from 'react';
import {Bar} from 'react-chartjs-2';

class BarChart extends Component {
    constructor(props){
        super(props);
        const data = props.chartData;
        const labels = Object.keys(data);
        const datasets = Object.values(data);
        this.state = {
            // chartData: props.chartData
            chartData: {
                // labels:['apple','Facebook','Amazon','Google','Microsoft'],
                labels: labels,
                datasets:[
                    {
                        label: 'Number of Tweets',
                        // data:[
                        //     data.apple.scores / data.apple.num,
                        //     data.facebook.scores / data.facebook.num,
                        //     data.amazon.scores / data.amazon.num,
                        //     data.google.scores / data.google.num,
                        //     data.microsoft.scores / data.microsoft.num
                        // ],
                        data: datasets,
                        // backgroundColor:[
                        //     'rgba(255, 99, 132, 0.2)',
                        //     'rgba(54, 162, 235, 0.2)',
                        //     'rgba(255, 206, 86, 0.2)',
                        //     'rgba(75, 192, 192, 0.2)',
                        //     'rgba(153, 102, 255, 0.2)',
                        // ],
                        backgroundColor: ["#F08080", "#dc3545","#ffc107","#007bff","#28a745"],
                        // borderColor: [
                        //     'rgba(255,99,132,1)',
                        //     'rgba(54, 162, 235, 1)',
                        //     'rgba(255, 206, 86, 1)',
                        //     'rgba(75, 192, 192, 1)',
                        //     'rgba(153, 102, 255, 1)',
                        //     'rgba(255, 159, 64, 1)'
                        // ],
                        borderWidth: 1,
                        hoverBackgroundColor: 'rgba(255,99,132,0.4)',
                        hoverBorderColor: 'rgba(255,99,132,1)',
                    }
                ]
            }
        }
    }
    
    static defaultProps = {
        displayTitle: true,
        displayLegend: true,
        legendPostion: 'bottom'
    }
    
    render(){
        return (
            <div className="chart">
                <Bar
                    data = {this.state.chartData}
                    options = {{
                        title: {
                            display: this.props.displayTitle,
                            text:'Sentiment Analysis',
                            fontSize: 25
                        },
                        legend: {
                            display: this.props.displayLegend,
                            position: this.props.legendPostion
                        },
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero: true
                                }
                            }]
                        }
                    }}
                />
            </div>
        )
    }
}


export default BarChart
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import axios from 'axios';

axios.defaults.baseURL = 'http://localhost:3000';
// axios.defaults.baseURL = 'http://cab432-ELB-364540564.ap-southeast-2.elb.amazonaws.com';

// axios.defaults.baseURL = 'http://cab432-asign2-elb-1507644489.ap-southeast-2.elb.amazonaws.com' |
// 'http://localhost:3000';

ReactDOM.render(<App />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();

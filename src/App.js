import React, { Component } from 'react';
import './App.css';
import Header from './Components/Header';
import Tweet from './Components/Tweet';

class App extends Component {
  render() {
    return (
      <div>
        <Header />
        <main>
          <Tweet />
        </main>
      </div>
    );
  }
}

export default App;

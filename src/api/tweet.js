import axios from 'axios';

// export function fetchTweets(){
//     return axios.get('/tweet').then(response => {
//         // further process
//         return response.data;
//     })
//     .catch(function (error) {
//         if (error.response) {
//             console.log(error.response.data);
//             console.log(error.response.status);
//             console.log(error.response.headers);
//         }
//     });
// } 

export function fetchTweets(term){
    return axios.get(`/tweet/${term}`).then(response=>response.data);
}

// export function fetchTweets(){
//     return axios.get('/tweet').then(response=>response.data);
// }
